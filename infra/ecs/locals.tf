locals {
	region = "eu-central-1"
	terraform_state_bucket = "terraform-backend-651380694328-${local.region}"
}