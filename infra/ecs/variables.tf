variable "environment" {
  type        = string
  description = "Logical name of the environment. Must be `dev` or `prod`."

  validation {
    condition     = contains(["dev", "prod"], var.environment)
    error_message = "Invalid environment name."
  }
}

# variable "cpu" {
#   type        = number
#   description = "CPU units for the ECS task."

#   validation {
#     condition     = contains([256, 512, 1024, 2048, 4096, 8192, 16384], var.cpu)
#     error_message = "Invalid value for ECS task's CPU units. Must be one of: 256, 512, 1024, 2048, 4096, 8192, 16384."
#   }
# }

# variable "memory" {
#   type        = number
#   description = "Memory for the ECS task. Split between all containers in the task."

#   validation {
#     condition     = var.memory == 512 || var.memory % 1024 == 0 && var.memory <= 122880
#     error_message = <<EOT
#     Invalid value for ECS task's memory.
#     Must be 512, 1024 or 2048 for 256 CPU units.
#     Between 1024 & 4096, in 1024 increments, for 512 CPU units.
#     Between 2048 & 8192, in 1024 increments, for 1024 CPU units.
#     Between 4096 & 16384, in 1024 increments, for 2048 CPU units.
#     Between 8192 & 30720, in 1024 increments, for 4096 CPU units.
#     Between 16384 & 61440, in 4096 increments, for 8192 CPU units.
#     Between 32768 & 122880, in 8192 increments, for 16384 CPU units.
#     EOT
#   }
# }

# # variable "container_image_tag" {
# #   type        = string
# #   description = "Tag of the container image in ECR repository to use for deployment."
# # }

# variable "desired_count" {
#   type        = number
#   description = "Number of instances of the task definition to place and keep running."
# }

# variable "deployment_minimum_healthy_percent" {
#   type        = number
#   description = "Lower limit (as a percentage of the service's desired_count) of the number of running tasks that must remain running and healthy in a service during a deployment."
#   default     = 50
# }

# variable "logs_retention" {
#   type        = number
#   description = "Logs retention period (in days) for service logs in CloudWatch."

#   validation {
#     condition     = contains([1, 3, 5, 7, 14, 30, 60, 90, 120, 150, 180, 365, 400, 545, 731, 1827, 3653, 0], var.logs_retention)
#     error_message = "Invalid retention period. Must be one of 1, 3, 5, 7, 14, 30, 60, 90, 120, 150, 180, 365, 400, 545, 731, 1827, 3653, and 0."
#   }
# }
