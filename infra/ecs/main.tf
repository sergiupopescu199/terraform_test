resource "aws_dynamodb_table" "terraform_locks" {
  name         = "terraform-state-locking"
  billing_mode = "PAY_PER_REQUEST"
  hash_key     = "LockID"
  attribute {
    name = "LockID"
    type = "S"
  }
}

# resource "aws_instance" "ec2_instance" {
#   ami = "ami-042e6fdb154c830c5"
#   instance_type = "t2.micro"

#   tags = {
# 	Name = "CiaoCacao"
#   }
# }

# # Definizione del gruppo di sicurezza per il microservizio
# resource "aws_security_group" "microservice_sg" {
#   name        = "microservice-sg"
#   description = "Security group for the microservice"
  
#   # Regole per il traffico in ingresso
#   ingress {
#     from_port   = 80
#     to_port     = 80
#     protocol    = "tcp"
#     cidr_blocks = ["0.0.0.0/0"]
#   }

#   ingress {
#     from_port   = 3306
#     to_port     = 3306
#     protocol    = "tcp"
#     cidr_blocks = ["0.0.0.0/0"]
#   }

#   # Regola per il traffico in uscita
#   egress {
#     from_port   = 0
#     to_port     = 0
#     protocol    = "-1"
#     cidr_blocks = ["0.0.0.0/0"]
#   }
# }

# # Definizione dell'istanza RDS MySQL
# resource "aws_db_instance" "mysql" {
#   identifier           = "microservice-db"
#   allocated_storage    = 20
#   storage_type         = "gp2"
#   engine               = "mysql"
#   engine_version       = "5.7"
#   instance_class       = "db.t2.micro"
#   name                 = "microservice_db"
#   username             = "admin"
#   password             = "password"
  
#   # Configurazione del gruppo di sicurezza
#   vpc_security_group_ids = [aws_security_group.microservice_sg.id]
# }

# # Definizione del WAF (Web Application Firewall)
# resource "aws_waf_web_acl" "microservice_waf" {
#   name        = "microservice-waf"
#   metric_name = "microservice-waf-metric"
  
#   # Inserire qui le regole WAF desiderate
# }

# # Definizione dell'Auto Scaling Group per il microservizio
# resource "aws_autoscaling_group" "microservice_asg" {
#   name             = "microservice-asg"
#   min_size         = 2
#   max_size         = 10
#   desired_capacity = 2
  
#   # Configurazione del gruppo di sicurezza
#   vpc_zone_identifier = ["subnet-12345678", "subnet-87654321"] # Inserire gli ID delle subnet desiderate
  
#   # Configurazione del launch configuration
#   launch_configuration = aws_launch_configuration.microservice_lc.name
  
#   # Configurazione delle metriche di scaling
#   metric {
#     namespace  = "AWS/ApplicationELB"
#     name       = "RequestCountPerTarget"
#     statistic  = "Average"
#     comparison_operator = "GreaterThanOrEqualToThreshold"
#     threshold  = 1000
#     unit       = "Count"
#   }
  
#   # Configurazione dell'azione di scaling
#   scaling_policy {
#     adjustment_type = "ChangeInCapacity"
#     cooldown        = 300
#     metric_aggregation_type = "Average"
#     name            = "microservice-scaling-policy"
#     adjustment_type = "ChangeInCapacity"
#     scaling_adjustment = 1
#   }
# }

# # Definizione del Launch Configuration per il microservizio
# resource "aws_launch_configuration" "microservice_lc" {
#   name_prefix   = "microservice-lc-"
#   image_id      = "ami-12345678" # Inserire l'ID AMI desiderato
#   instance_type = "t2.micro"
  
#   # Configurazione dello script di avvio
#   user_data = <<-EOF
#               #!/bin/bash
#               # Inserire qui gli script di avvio del microservizio
#               EOF
  
#   # Configurazione delle security groups
#   security_groups = [aws_security_group.microservice_sg.name]
# }

# # Definizione del Load Balancer per il microservizio
# resource "aws_lb" "microservice_lb" {
#   name               = "microservice-lb"
#   internal           = false
#   load_balancer_type = "application"
#   security_groups    = [aws_security_group.microservice_sg.id]
  
#   # Configurazione del listener
#   listener {
#     port            = 80
#     protocol        = "HTTP"
    
#     # Configurazione del target group
#     default_action {
#       type             = "forward"
#       target_group_arn = aws_lb_target_group.microservice_tg.arn
#     }
#   }
# }

# # Definizione del target group per il microservizio
# resource "aws_lb_target_group" "microservice_tg" {
#   name     = "microservice-tg"
#   port     = 80
#   protocol = "HTTP"
  
#   # Configurazione del health check
#   health_check {
#     path                = "/"
#     port                = "80"
#     protocol            = "HTTP"
#     timeout             = 5
#     interval            = 30
#     healthy_threshold   = 2
#     unhealthy_threshold = 2
#   }
  
#   # Configurazione dei target
#   # Inserire qui gli ID delle istanze o dei gruppi di Auto Scaling
# }