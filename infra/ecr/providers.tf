terraform {
  required_version = ">= 1.8"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.0"
    }
  }

  backend "s3" {
    bucket         = "terraform-backend-651380694328-eu-central-1"
    key            = "infra/ecr/terraform.tfstate"
    dynamodb_table = "terraform-state-locking"
    region         = "eu-central-1"
  }
}

provider "aws" {
  region = "eu-central-1"
}
