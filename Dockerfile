# Use an OpenJDK base image for building the application
FROM maven:3.8.4-openjdk-11 AS build

# Set the working directory in the container
WORKDIR /app

# Copy the Maven project file and download dependencies
COPY pom.xml .

# Copy the entire source code
COPY src/ /app/src/

# Build the application
RUN mvn clean package

# Use a lightweight OpenJDK JRE base image for running the application
FROM openjdk:11-jre-slim

# Set the working directory in the container
WORKDIR /app

# Copy the built JAR file from the build stage
COPY --from=build /app/target/SpringBootHelloRest-0.0.1-SNAPSHOT.jar /app/SpringBootHelloRest.jar

# Expose the port that the Spring Boot application listens on
EXPOSE 8080

# Run the Spring Boot application when the container starts
CMD ["java", "-jar", "SpringBootHelloRest.jar"]
